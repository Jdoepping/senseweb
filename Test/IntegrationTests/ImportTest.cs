﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using SenseWeb.AnyVision.ApiClasses;
using Xunit;

namespace Test.IntegrationTests
{
    partial class AnyVisionServiceTest
    {
        /// <summary>
        /// Part of a mass import is a preparation that names the import and specifies
        /// the groups into which the subjects should be imported. 
        /// </summary>
        /// <returns></returns>
        [Fact]
        [Trait("Category", "Import")]
        public async Task PrepareMassImportTest()
        {
            // Arrange
            var group = await GetAGroup(service,true);
            var prepare = new MassImportRequest
            {
                Name = "MassImport_" + DateTime.Now.ToString(CultureInfo.InvariantCulture).Substring(0),
                SubjectGroups = new List<string> { group.Id },
            };
            // Act
            var res = await service.Import.PrepareMassImport(prepare);
            // Assert
            // Todo: Missing assert
        }
        [Fact]
        [Trait("Category", "Import")]
        public async Task ImportSingleSubjectToGroupTest()
        {
            // Arrange
            var group = await GetAGroup(service, true);
            var subjects = await service.Subjects.GetFromGroup(group.Id);
            // Act
            await AddSubjectToGroup(group.Id);
            // Assert
            var subjectsAfter = await service.Subjects.GetFromGroup(group.Id);
            Assert.Equal(subjects.Count() + 1, subjectsAfter.Count());
        }

        private async Task AddSubjectToGroup(string groupId)
        {
            var faces = await service.ExtractFacesFromFile(SingleImageFile);
            // The response from ExtractFaces is the class FaceExtractResponse
            Assert.True(faces.Items.Any());
            var first = faces.Items.First();
            var body = new AddSubject
            {
                Name = "Name_05",
                SubjectSource = 1, // 1= Watchlist
                Description = "Description_01",
                IsIgnored = false,
                Groups = new List<string> { groupId },
                Images = new List<Image> {new Image
                {
                    Url = first.Url,
                    Features = first.Features,
                    IsPrimary = true,
                    //Backup = new List<Backup>(),
                    FeaturesQuality = faces.Items.First().FeaturesQuality,
                    ObjectType = 1, // Face
                    ValidUntil = DateTime.Now,
                    //Backup = new List<Backup>(),
                }},
                //TrackId = "",
                //FrameTimeStamp = DateTime.Now,
                SearchBackwards = new List<SearchBackward>(),
            };
            var response = await service.Subjects.Add(body);
        }
    }
}
