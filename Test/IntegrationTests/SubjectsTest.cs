﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SenseWeb.AnyVision.ApiClasses;
using SenseWeb.AnyVision.Generated.Group;
using Xunit;

namespace Test.IntegrationTests
{
    /// <summary>
    /// Tests involving subjects. Most of these test assumes subjects to already
    /// by present.
    ///
    /// Subjects belongs to groups
    /// </summary>
    public partial class AnyVisionServiceTest
    {
        [Fact]
        [Trait("Category", "Subject")]
        public async Task GetSubjectsTest()
        {
            var subjects = await service.Subjects.Get();
            Assert.True(subjects.Items.Any());
        }

        [Fact]
        [Trait("Category", "Subject")]
        public async Task BulkDeleteSubjectsTest()
        {
            var subjects = await service.Subjects.Get();
            if (subjects.Items.Count <= 2) Assert.False(true, "Not enough subjects present to be able to delete two. Add some...");
            var toBeDeleted = subjects.Items.Take(2).Select(t => t.Id).ToList();

            var res = await service.Subjects.Delete(toBeDeleted.ToArray());
            var afterDeleteSubjects = await service.Subjects.Get();
            Assert.True(subjects.Items.Count ==(afterDeleteSubjects.Items.Count+2));
        }

        [Fact]
        [Trait("Category", "Subject")]
        public async Task ReGroupTest()
        {
            // Arrange
            var subjects = await service.Subjects.Get();
            var groups = await service.Groups.GetAll();
            // Take the first subject and move to a different group
            var subjectToBeMoved = subjects.Items.First();
            var notMemberOfGroup = groups.Items.First(g=>!subjectToBeMoved.Groups.Select(t=>t.Id).Contains(g.Id));
            testOutputHelper.WriteLine($"Move subject with name={subjectToBeMoved.Name} and id={subjectToBeMoved.Id}");
            testOutputHelper.WriteLine($"To group with name={notMemberOfGroup.Title}");
            var groupsIds = new List<string> { subjectToBeMoved.Groups.First().Id, notMemberOfGroup.Id };
            var subjectIds = new List<string> { subjectToBeMoved.Id };
            // Act
            var response = await service.Subjects.ReGroupSubjects(groupsIds, subjectIds);
            //Assert

        }

        /// <summary>
        /// Updates values for a subject. 
        /// An image is required!
        /// Green on 4/1/2021
        /// </summary>
        /// <returns></returns>
        [Fact]
        [Trait("Category", "Subject")]
        public async Task PatchASubjectTest()
        {
            var faces = await service.ExtractFacesFromFile(SingleImageFile);
            // The response from ExtractFaces is the class FaceExtractResponse
            Assert.True(faces.Items.Any());
            var first = faces.Items.First();
            // Arrange
            var groups = await service.Groups.GetAll();
            var group = default(Item);
            var subject= new SenseWeb.AnyVision.Generated.Subject.Item();
            foreach (var g in groups.Items)
            {
                if (g.IsDefault) continue;
                var subjects = await service.Subjects.GetFromGroup(g.Id);
                if (!subjects.Any()) continue;
                group = g;
                subject = subjects.First();
                break;
            }

            if (group is default(Item)) Assert.False(true, "No group has a subjects attached");
            Assert.NotNull(subject);
            var body = new PatchSubject
            {
                Name = "Patched Name",
                Description = "Added a description",
                Groups = new List<string> { group.Id},
                Images = new List<Image> {new Image
                {
                    Url = first.Url,
                    Features = first.Features,
                    IsPrimary = true,
                    //Backup = new List<Backup>(),
                    FeaturesQuality = faces.Items.First().FeaturesQuality,
                    ObjectType = 1, // Face
                    ValidUntil = DateTime.Now,
                    //Backup = new List<Backup>(),
                }},
            };
            // Act
            var res = await service.Subjects.Patch(subject.Id, body);
            // Assert
           
        }


    }
}
