﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using SenseWeb.AnyVision.Service;
using Xunit;
using Xunit.Abstractions;

namespace Test.IntegrationTests
{
    public partial class AnyVisionServiceTest: IClassFixture<WebApplicationFactory<SenseWeb.Startup>>
    {
        public const string WorkingZipFile = @"C:\Users\jdoep\Pictures\SenseAndCoin\uploadit.zip";
        public const string SingleImageFile = @"C:\Users\jdoep\Pictures\SenseAndCoin\singleperson.jfif";

        private readonly ITestOutputHelper testOutputHelper;
        private readonly WebApplicationFactory<SenseWeb.Startup> factory;
        private readonly Settings settings;
        private readonly Service service;
        public AnyVisionServiceTest(WebApplicationFactory<SenseWeb.Startup> factory, ITestOutputHelper testOutputHelper)
        {
            this.factory = factory;
            this.testOutputHelper = testOutputHelper;
            settings = new Settings();
            service = (Service)factory.Services.GetService(typeof(Service));
            service.Log = testOutputHelper.WriteLine;
        }

        /// <summary>
        /// This is also a login request. A successful login returns a token.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task GetTokenTest()
        {
            var response = await service.Connect(settings.UserName, settings.Password);
            Assert.True(response.IsSuccessStatusCode, response.StatusCode.ToString());
        }

        [Fact]
        public async Task GetCamerasTest()
        {
            var cameras = await service.Cameras();
            Assert.True(cameras.Items.Any());
        }

        [Fact]
        public async Task ExtractFromImageTest()
        {
            var faces = await service.ExtractFacesFromFile(SingleImageFile);
            Assert.True(faces.Items.Any());
        }

        
        public class Settings //: IAnyVisionSettings
        {
            public string UserName => "jakob";

            public string Password => "Anyvision1!";

            public string BaseUrl => "http://172.20.48.78";
        }

        /// <summary>
        /// Selects a group. If the parameter <see cref="exclude"/> is true the default group is excluded.
        /// </summary>
        ///  <param name="service"></param>
        /// <param name="exclude">Exclude the default group when true</param>
        /// <returns>Returns a random group.</returns>
        public static async Task<SenseWeb.AnyVision.Generated.Group.Item> GetAGroup(Service service ,bool exclude)
        {
            var groups = await service.Groups.GetAll();
            return exclude ? groups.Items.First(t => !t.IsDefault) : groups.Items.First();
        }
    }
}
