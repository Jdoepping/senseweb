﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SenseWeb.AnyVision.Generated.Group;
using Xunit;

namespace Test.IntegrationTests
{
   public  partial class AnyVisionServiceTest
    {
        [Fact]
        [Trait("Category", "Group")]
        public async Task GetGroupsTest()
        {
            var groups = await service.Groups.GetAll();
            Assert.True(groups.Items.Any());
        }

        [Fact]
        [Trait("Category", "Group")]
        public async Task CreateGroupTest()
        {
            var group = CreateGroup();
            var response = await service.Groups.Create(group);
            //Todo:
            //Assert.True(response.IsSuccessStatusCode, response.StatusCode.ToString());
        }

        [Fact]
        [Trait("Category", "Group")]
        public async Task DeleteGroupTest()
        {
            // Arrange
            var createdGroup = await service.Groups.Create(CreateGroup());
            var fetchedGroup= await service.Groups.GetById(createdGroup.Id);
            // make sure it is created
            Assert.Equal(createdGroup.Id, fetchedGroup.Id);

            // Act
            var success = await service.Groups.DeleteById(createdGroup.Id);

            // Assert
            Assert.True(success);
            await Assert.ThrowsAsync<System.Net.Http.HttpRequestException>(async ()=>await service.Groups.GetById(createdGroup.Id));
        }

        [Fact]
        [Trait("Category", "Group")]
        public async Task ClearSubjectFromGroupTest()
        {
            // Arrange
            var createdGroup = await service.Groups.Create(CreateGroup());
            // Add subjects
            await AddSubjectToGroup(createdGroup.Id);
            await AddSubjectToGroup(createdGroup.Id);
            var subjects = await service.Subjects.GetFromGroup(createdGroup.Id);
            Assert.True(subjects.Count() == 2);

            //Act
            var success = await service.Groups.ClearAllSubjects(createdGroup.Id);

            // Assert
            Assert.True(success);
            subjects = await service.Subjects.GetFromGroup(createdGroup.Id);
            Assert.True(!subjects.Any());

            // clean up
            await service.Groups.DeleteById(createdGroup.Id);
        }

        private CreateGroup CreateGroup() => new CreateGroup
        {
            alertLevel = "00000000-0200-48f3-b728-10de4c0a906f",
            color = "#ff11f7",
            description = "Created by Jad Group",
            title = "Group 001",
            th = 0,
            type = 0
        };
    }
}
