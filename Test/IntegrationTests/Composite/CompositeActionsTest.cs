﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using SenseWeb.AnyVision;
using SenseWeb.AnyVision.Service;
using Xunit;
using Xunit.Abstractions;

namespace Test.IntegrationTests.Composite
{
    public class CompositeActionsTest : IClassFixture<WebApplicationFactory<SenseWeb.Startup>>
    {
        private readonly ITestOutputHelper testOutputHelper;
        private readonly WebApplicationFactory<SenseWeb.Startup> factory;
        private readonly AnyVisionServiceTest.Settings settings;
        private readonly Service service;

        private readonly CompositeActions sut;

        public CompositeActionsTest(WebApplicationFactory<SenseWeb.Startup> factory, ITestOutputHelper testOutputHelper)
        {
            this.factory = factory;
            this.testOutputHelper = testOutputHelper;
            settings = new AnyVisionServiceTest.Settings();
            service = (Service) factory.Services.GetService(typeof(Service));
            service.Log = testOutputHelper.WriteLine;
            sut = new CompositeActions(service, testOutputHelper.WriteLine);
        }

        /// <summary>
        /// Calling clean shall remove all groups and subjects
        /// </summary>
        /// <returns></returns>
        [Fact]
        [Trait("Category", "Composite")]
        public async Task CleanTest()
        {
            // Arrange
            var groups = await service.Groups.GetAll();
            Assert.True(groups.Items.Count > 1, "Less than one group");
            //var subjects = await service.Subjects.Get();
            //Assert.True(subjects.Items.Count > 0, "subject count is zero");

            // Act
            await sut.Clean();

            // Assert
            groups = await service.Groups.GetAll();
            Assert.True(groups.Items.Count == 1, "Group count is not one");
        }

        /// <summary>
        /// Uploads the images found in the file named AnyVisionServiceTest.WorkingZipFile.
        /// This file contains 2 images Ross and Keanu and will add two subjects.
        /// </summary>
        /// <returns></returns>
        [Trait("Category", "Composite")]
        [Fact]
        public async Task UploadCompressedFileTest()
        {
            // Arrange
            var group = await AnyVisionServiceTest.GetAGroup(service, true);
            // Act
            var (result, count) = await sut.UploadGamblers(AnyVisionServiceTest.WorkingZipFile, new List<string> {group.Id});
            // Assert
            Assert.True(result);
        }
    }
}
