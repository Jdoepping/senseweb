﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SenseWeb.Synch;
using SenseWeb.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace Test.FileSystemTest
{
    /// <summary>
    /// The tests will make changes to the file system.
    /// Removing all folders and files in a folder.
    /// </summary>
    public class FileSystemTest: IClassFixture<FileSystemFixture>
    {
        private const string TestPathParent = @"C:\Temp\DeleteTest";
        private const string TestPath = @"C:\Temp\DeleteTest\Delete";
        private readonly ITestOutputHelper testOutputHelper;
        private readonly IFileSystem fileSystem;
        private readonly CancellationToken token;
        public FileSystemTest(FileSystemFixture fileSystem, ITestOutputHelper testOutputHelper)
        {
            this.testOutputHelper = testOutputHelper;
            this.fileSystem = fileSystem;
            if (!Directory.Exists(TestPathParent))
            {
                Directory.CreateDirectory(TestPathParent);
            }
            var source = new CancellationTokenSource();
            token = source.Token;
        }

        [Fact]
        public async Task SaveToFolderTest()
        {
            // Arrange
            Assert.True(Directory.Exists(fileSystem.FolderPath));
            await fileSystem.FolderDeleteAll();
            Assert.False(Directory.Exists(fileSystem.FolderPath));
            var testData = TestData();

            // Act
            await fileSystem.SaveToFolder(testData, token);

            // Assert
            Assert.True(Directory.Exists(fileSystem.FolderPath));
            Assert.True(Directory.EnumerateDirectories(fileSystem.FolderPath).Count()==testData.Count());
            foreach (var dir in Directory.EnumerateDirectories(fileSystem.FolderPath))
            {
                Assert.True(Directory.EnumerateFiles(dir).Count() == 1);
            }
        }

        private IEnumerable<IGambler> TestData()
        {
            var gamblers = new List<IGambler>();
            for (var i = 0; i < 10; i++)
            {
                var gambler = new Gambler
                {
                    DateOfBirth = DateTime.Now.AddYears(-(i * 10)),
                    FirstName = $"First Name 0{i}",
                    LastName = $"Last Name 0{i}",
                    Images = new List<byte[]> {new byte[] {0x0, 0x1, 0x2, 0x3}},

                };
                gamblers.Add(gambler);
            }
            return gamblers;
        }


    }
}
