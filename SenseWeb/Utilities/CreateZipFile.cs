﻿using System;
using System.IO.Compression;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SenseWeb.Utilities
{
    public static class Zip
    {
        public static bool ZipDirectory(string directory)
        {
            var path = Path.Combine(directory);
            var destination = @"C:\Users\jdoep\Pictures\SenseAndCoin\zipTest.zip";
            ZipFile.CreateFromDirectory(directory,destination,CompressionLevel.Fastest,false);
            return true;
        }
    }
}

