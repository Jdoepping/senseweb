﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SenseWeb.Synch;
using SixLabors.ImageSharp;

namespace SenseWeb.Utilities
{
    /// <summary>
    /// Encapsulates some functionality on a file system.
    /// Images are saved to the file system for the purpose of creating a zip file of the directory structure
    /// and images. 
    /// </summary>
    public class FileSystem : IFileSystem
    {
        private readonly Func<byte[], CancellationToken, Task> saveImage;

        /// <param name="path"></param>
        /// <param name="saveImage">when null defaults to a locally created method</param>
        public FileSystem(string path, Func<byte[], CancellationToken, Task>? saveImage) =>
            (FolderPath, this.saveImage) = (path, (saveImage ?? SaveImage.Save));
        public string FolderPath { get; }

        public async Task FolderDeleteAll()
        {
            if (!Directory.Exists(FolderPath)) return;
            Directory.Delete(FolderPath, true);
        }

        public async Task SaveToFolder(IEnumerable<IGambler> gamblers, CancellationToken token)
        {
            if (!Directory.Exists(FolderPath))
                Directory.CreateDirectory(FolderPath);
            foreach (var gambler in gamblers)
            {
                var dir = Directory.CreateDirectory($"{FolderPath}/{gambler.FirstName}_{gambler.LastName}");
                var i = 0;
                foreach (var gamblerImage in gambler.Images)
                {
                    token.ThrowIfCancellationRequested();
                    File.WriteAllBytes(dir.FullName+$"/image0{i++}.jpg", gamblerImage);
                    //await saveImage(gamblerImage, token);
                }
            }
        }
        /// <summary>
        /// Use SixLabors.ImageSharp to save a byte array as a jpeg.
        /// </summary>
        private static class SaveImage
        {
            public  static async Task Save(byte[] image, CancellationToken token)
            {
                await using var inStream = new MemoryStream();
                await using var outStream = new MemoryStream();
                using var imageSharp = await Image.LoadAsync(inStream);
                await imageSharp.SaveAsJpegAsync(outStream, token);
            }
        }
    }
    
}
