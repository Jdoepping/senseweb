﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using SenseWeb.AnyVision;

namespace SenseWeb.Synch
{
    public class SynchronizationService : ISynchronizationService
    {
        private readonly string folderPath;
        private readonly ISourceSystem sourceSystem;
        private readonly ITargetSystem anyVisionService;

        public SynchronizationService(ISourceSystem service, ITargetSystem anyVisionService, string folderPath) =>
            (sourceSystem, this.anyVisionService, this.folderPath) = (service, anyVisionService, folderPath);

        public async Task<(bool,string)> InitialSynchronization()
        {
            // Todo: gamblers should be saved on the file system, and a zipFile created.
            var gamblers = await sourceSystem.GetGamblers();
            // Todo: knowing where gamblers are saved and how the zipfile is named is a sort of shared knowledge?
            // Are something the target system knows and can share?
            // Look down and see the AnyVisionService.
            var (status, errMessage) = await anyVisionService.InitialSynchronization(gamblers);
            return (true, "");
        }
    }

    public interface IAnyVisionService
    {
        Task Synchronize(IEnumerable<IGambler> gamblers, CancellationToken token);
    }

    public interface IZip
    {
        Task<string> Create(string folderPath);
    }

    public interface IFileSystem
    {
        Task SaveToFolder(IEnumerable<IGambler> gamblers, CancellationToken token);
        /// <summary>
        /// Delete everything in the folder
        /// </summary>
        /// <returns></returns>
        Task FolderDeleteAll();
        string FolderPath { get; }
    }

    public class AnyVisionService : IAnyVisionService
    {
        private readonly IFileSystem fileSystem;
        private readonly IZip zip;
        private readonly CompositeActions compositeActions;

        public AnyVisionService(IFileSystem fileSystem, IZip zip, AnyVision.Service.Service service, Action<string> log) =>
            (this.fileSystem, this.zip, compositeActions) = (fileSystem, zip, new CompositeActions(service, log));

        public async Task Synchronize(IEnumerable<IGambler> gamblers, CancellationToken token)
        {
            await fileSystem.FolderDeleteAll();
            await fileSystem.SaveToFolder(gamblers, token);
            var zipFile = await zip.Create(fileSystem.FolderPath);
            // Todo: empty list is a bug
            await compositeActions.UploadGamblers(zipFile, new List<string>());
        }
    }
}
