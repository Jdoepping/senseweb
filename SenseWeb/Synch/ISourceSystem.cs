﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SenseWeb.Synch
{
    /// <summary>
    /// Interface representing the source system in the integration.
    /// The actual source system is self exclusion system for gamblers. So, to avoid
    /// to much abstraction the interface will reflect that.
    /// 
    /// A service that provides access to data about gamblers who have opted for self exclusion.
    /// </summary>
    public interface ISourceSystem
    {
        Task<IEnumerable<IGambler>> GetGamblers();
    }
}
