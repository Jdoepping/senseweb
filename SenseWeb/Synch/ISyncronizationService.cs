﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SenseWeb.Synch
{
    /// <summary>
    /// This interface contains the processes that can be executed.
    /// Involved with exchanging data between the source and target systems
    /// </summary>
    public interface ISynchronizationService
    {
        /// <summary>
        /// Initial copying of data from an exclusion database to AnyVision.
        /// </summary>
        /// <returns></returns>
        Task<(bool, string)> InitialSynchronization();
    }
}
