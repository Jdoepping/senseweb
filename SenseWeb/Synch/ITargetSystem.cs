﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SenseWeb.Synch
{
    /// <summary>
    /// The overall abstract task is a synchronization between to systems: Source and target.
    /// To be specific:
    /// The source systems are Coin and Sense
    /// The target system is AnyVision
    ///
    /// This interface specifies the operations the target system should support.
    /// 
    /// This class implements the below when it relates to AnyVision (BT Better Tomorrow)
    /// 
    /// 1.1 Ensure all watchlist subjects and groups are deleted. 
    /// 1.2 Initial Synchronization with SENSE Database.
    /// 1.3 Delete several subjects.                                            Status: OK
    /// 1.4 Re-synchronization successful.
    /// 1.5 Move a subject from a watchlist to a different watchlist.           Status: AnyVision doesn't work
    /// 1.6 Re-synchronization successful.
    /// 1.7 Edit subject parameters.
    /// 1.8 Re-synchronization successful.
    /// 1.9 Create an error within the SENSE database (no picture, no name, etc).
    /// 1.10 Re-synchronization error generated.
    /// 1.11 Delete subjects from the SENSE database.
    /// 1.12 Re-synchronization successful.
    /// 1.13 Break the connection between BT and SENSE database.
    /// 1.14 Re-synchronization error generated.
    /// 1.15 Add a new subject within BT                                        Status: Currently AnyVision gives license error used to work
    /// 1.16 Re-synchronization successful.
    /// </summary>
    public interface ITargetSystem
    {
        /// <summary>
        /// 1.1 Ensure all watchlist subjects and groups are deleted.
        ///
        /// This revolves around the notion of a "WatchList". A watchlist is a list of gamblers or persons.
        /// The gamblers are identified by a picture an personal data (name, sex etc)
        ///
        /// In Anyvision a watchlist contains subjects (gamblers, persons) and are organized in groups.
        /// 
        /// 
        /// </summary>
        Task<(bool, string)> Clean();

        /// <summary>
        /// 1.2 Initial Synchronization
        /// Moving all subjects (gamblers) from the source to the target system. 
        /// </summary>
        /// <returns></returns>
        Task<(bool, string)> InitialSynchronization(IEnumerable<IGambler> gamblers);
        /// <summary>
        /// 1.3 Delete several subjects
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<(bool, string)> Remove(IEnumerable<string> ids);
        /// <summary>
        /// 1.5 Move a subject from a watchlist to a different watchlist.
        ///
        /// The term "Watchlist" is an AnyVision term a kind of grouping, like folders.
        /// In AnyVision a watchlist is also a group. The structure is single level.
        /// But keeping with the analogy of a folder should work
        /// </summary>
        /// <param name="sourceId">uuid identifying the source folder (group in AnyVision terminology)</param>
        /// <param name="targetId">uuid identifying the target folder (group in AnyVision terminology)</param>
        /// <param name="ids">list of items to be moved (subjects in AnyVision terminology)</param>
        /// <returns></returns>
        Task<(bool, string)> Move(string sourceId, string targetId, IEnumerable<string> ids);
        /// <summary>
        /// Edit subject parameters.
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        Task<(bool, string)> EditSubject(string name, string description, byte[] image);
        /// <summary>
        /// 1.15 Add a new subject within BT
        ///
        /// Add a single object. Contrast it with InitialSynchronization, this method
        /// will add many subjects. 
        /// </summary>
        ///  <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        Task<(bool, string)> Add(string name, string description, byte[] image);
    }
}
