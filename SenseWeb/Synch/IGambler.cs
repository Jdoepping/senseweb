﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SenseWeb.Synch
{
    public interface IGambler
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        DateTime DateOfBirth { get; set; }
        List<Byte[]> Images { get; set; }
    }

    public class Gambler : IGambler
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public List<byte[]> Images { get; set; }
    }

    //this.columnmiddlenames = base.Columns["middlenames"];
    //this.columntitle = base.Columns["title"];
    //this.columnsex = base.Columns["sex"];
    //this.columndateofbirth = base.Columns["dateofbirth"];
    //this.columnaddress = base.Columns["address"];
    //this.columnpostal_code = base.Columns["postal_code"];
    //this.columncountry = base.Columns["country"];
    //this.columntelephone = base.Columns["telephone"];
    //this.columnemail = base.Columns["email"];
    //this.columnregion_id = base.Columns["region_id"];
    //this.columnsignature = base.Columns["signature"];
    //this.columncasino_id = base.Columns["casino_id"];
    //this.columnoriginal_photo = base.Columns["original_photo"];
    //this.columnphoto120 = base.Columns["photo120"];
    //this.columnphoto300 = base.Columns["photo300"];
    //this.columnphoto500 = base.Columns["photo500"];
    //this.columnphoto1000 = base.Columns["photo1000"];
}
