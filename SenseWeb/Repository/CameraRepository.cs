﻿//using System.Threading.Tasks;
//using Sense.AnyVision.Generated.Models.Cameras;
//using SenseWeb.AnyVision;

//namespace SenseWeb.Repository
//{
//    public interface ICameraRepository
//    {
//        Task<Root> GetCameras(int offset, int sortOrder, int limit);
//    }

//    public class CameraRepository : ICameraRepository
//    {
//        private readonly IAnyVisionHttpClient _client;

//        public CameraRepository(IAnyVisionHttpClient client)
//        {
//            _client = client;
//        }

//        public async Task<Root> GetCameras(int offset, int sortOrder, int limit)
//        {
//            var result = await _client.Get<Root>($"cameras?offset={offset}&sortOrder={sortOrder}&limit={limit}");
//        }
//    }
//}
