﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace SenseWeb.AnyVision.ApiClasses
{
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public class AddSubject
    {
        [JsonProperty(PropertyName="name")]

        public string Name { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        /// <summary>
        /// Watchlist = 1, Track = 2
        /// </summary>
        [JsonProperty(PropertyName="subjectSource")]
        public int SubjectSource { get; set; }
        [JsonProperty(PropertyName="isIgnored")]
        public bool IsIgnored { get; set; }
        [JsonProperty(PropertyName="groups")]
        public  List<string> Groups { get; set; }
        [JsonProperty(PropertyName="images")]
        public List<Image> Images { get; set; }
        //[JsonProperty(PropertyName="trackId")]
        //public string TrackId { get; set; }
        //[JsonProperty(PropertyName="frameTimeStamp")]
        //public DateTime FrameTimeStamp { get; set; }
        [JsonProperty(PropertyName="searchBackwards")]
        public List<SearchBackward> SearchBackwards { get; set; }
    }

    public class Image
    {
        [JsonProperty(PropertyName="url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName="featuresQuality")]
        public int FeaturesQuality { get; set; }

        [JsonProperty(PropertyName="isPrimary")]
        public bool IsPrimary { get; set; }

        /// <summary>
        /// Face= 1, Body =2
        /// </summary>
        [JsonProperty(PropertyName="objectType")]
        public int ObjectType { get; set; }

        [JsonProperty(PropertyName="features")]
        public List<double> Features { get; set; }

        /// <summary>
        /// Format example
        /// "2021-03-24T05:02:10.000Z"
        /// </summary>
        [JsonProperty(PropertyName="validUntil")]
        public DateTime ValidUntil { get; set; }

        //[JsonProperty(PropertyName="backup")]
        //public List<Backup> Backup { get; set; }

        //[JsonPropertyName("landmarkScore")]
        //public int LandmarkScore { get; set; }

        //[JsonPropertyName("attributes")]
        //public Attributes Attributes { get; set; }
    }

    public class Backup
    {
        /// <summary>
        /// Face= 1, Body =2
        /// </summary>
        [JsonProperty(PropertyName="objectType")]
        public int ObjectType { get; set; }
       
        [JsonProperty(PropertyName="originalName")]
        public string OriginalName { get; set; }

        [JsonProperty(PropertyName="path")]
        public Uri Path { get; set; }

        [JsonProperty(PropertyName="position")]
        public List<Position> Position { get; set; }
    }

    public class Position
    {
        [JsonProperty(PropertyName="left")]
        public string Left { get; set; }
        [JsonProperty(PropertyName="right")]
        public string Right { get; set; }
        [JsonProperty(PropertyName="top")]
        public string Top { get; set; }
        [JsonProperty(PropertyName="bottom")]
        public string Bottom { get; set; }
    }

    public class SearchBackward
    {
        [JsonProperty(PropertyName="searchBackwardsThreshold")]
        public float SearchBackwardsThreshold { get; set; }
        [JsonProperty(PropertyName="objectType")]
        public int ObjectType { get; set; }
    }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
}
