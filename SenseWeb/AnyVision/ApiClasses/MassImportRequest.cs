﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SenseWeb.AnyVision.ApiClasses
{
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public class MassImportRequest
    {
        private string name;
        /// <summary>
        /// The length of this property is limited to 30 characters.
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name
        {
            get => name;
            // todo: perhaps throw an error when the limit is exceeded?
            set => name = (value.Length>30)? value.Substring(0,30) : value;
        }

        [JsonProperty(PropertyName = "subjectGroups")]
        public List<string> SubjectGroups { get; set; }
        /// <summary>
        /// Watchlist = 1, Track = 2
        /// </summary>
        [JsonProperty(PropertyName = "isSearchBackwards")]
        public bool IsSearchBackwards { get; set; } = false;

        /// <summary>
        /// Threshold should not be included when <see cref="IsSearchBackwards"/> is false
        /// </summary>
        [JsonProperty(PropertyName = "threshold")]
        public double Threshold { get; set; } = 0;
    }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
}
