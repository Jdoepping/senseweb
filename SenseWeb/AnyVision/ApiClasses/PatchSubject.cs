﻿using System.Collections.Generic;
using Newtonsoft.Json;
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
namespace SenseWeb.AnyVision.ApiClasses
{
    public class PatchSubject
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "groups")]
        public List<string> Groups { get; set; }
        [JsonProperty(PropertyName = "images")]
        public List<Image> Images { get; set; }
    }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
}
