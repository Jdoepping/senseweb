﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
namespace SenseWeb.AnyVision.ApiClasses
{
    public class UploadCompressedFile
    {
        /// <summary>
        /// path and filename.
        /// </summary>
        [JsonProperty(PropertyName = "file")]
        public string FileName { get; set; }
    }

}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
