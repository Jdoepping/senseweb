﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SenseWeb.AnyVision.interfaces
{
    public interface IAnyVisionHttpClient
    {
        Task<T> PostAsync<T>(string requestUri, HttpContent content);
        Task<T> GetAsync<T>(string requestUri);
        Task<T> DeleteAsync<T>(string requestUri);
        Task<TR> PostAsJsonAsync<T,TR>(string requestUri, T param);
        Task<TR> PutAsJsonAsync<T,TR>(string requestUri, T param);
        Task<TR> PatchAsJsonAsync<T, TR>(string requestUri, T param);
        Action<string> Log { get; set; }
    }
}
