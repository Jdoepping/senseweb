﻿using System;
using System.Threading.Tasks;

namespace SenseWeb.AnyVision
{
    /// <summary>
    /// A token must be set on most calls.
    /// A token has a lifetime. Reusing the token reduces calls to the service.  
    /// </summary>
    public class TokenHandler
    {
        private  TimeSpan TokenLifeSpan { get; } 
        private DateTime CreateTime { get; set; }
        private string Token { get; set; } = "";
        public TokenHandler(TimeSpan? lifeSpan = null)
        {
            TokenLifeSpan = lifeSpan ?? TimeSpan.FromHours(1);
            CreateTime = DateTime.MinValue;
        }
        public async Task AddToken(Func<Task<string>> getToken, Action<string> addToken)
        {
            if (HasExpired()) await RenewToken(getToken);
            addToken(Token);
        }
        private bool HasExpired() => (DateTime.Now - CreateTime) > TokenLifeSpan;
        private async Task RenewToken(Func<Task<string>> getToken)
        {
            Token = await getToken();
            CreateTime = DateTime.Now;
        }
    }
}
