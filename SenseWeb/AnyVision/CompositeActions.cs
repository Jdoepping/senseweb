﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using SenseWeb.AnyVision.ApiClasses;

namespace SenseWeb.AnyVision
{
    /// <summary>
    /// This class implements the below when it relates to AnyVision (BT Better Tomorrow)
    /// 
    /// 1.1 Ensure all watchlist subjects and groups are deleted. 
    /// 1.2 Initial Synchronization with SENSE Database.
    /// 1.3 Delete several subjects.                                            Status: OK
    /// 1.4 Re-synchronization successful.
    /// 1.5 Move a subject from a watchlist to a different watchlist.           Status: AnyVision doesn't work
    /// 1.6 Re-synchronization successful.
    /// 1.7 Edit subject parameters.
    /// 1.8 Re-synchronization successful.
    /// 1.9 Create an error within the SENSE database (no picture, no name, etc).
    /// 1.10 Re-synchronization error generated.
    /// 1.11 Delete subjects from the SENSE database.
    /// 1.12 Re-synchronization successful.
    /// 1.13 Break the connection between BT and SENSE database.
    /// 1.14 Re-synchronization error generated.
    /// 1.15 Add a new subject within BT                                        Status: Currently AnyVision gives license error used to work
    /// 1.16 Re-synchronization successful.

    /// </summary>
    public class CompositeActions
    {
        private readonly SenseWeb.AnyVision.Service.Service service;

        public CompositeActions(SenseWeb.AnyVision.Service.Service service, Action<string> log) =>
            (this.service, Log) = (service, log);

        public Action<string> Log { get; }

        /// <summary>
        /// Removes all subjects and groups
        /// NB There are Rules a group must be empty before it can be deleted
        /// NB Currently the Default group is not deleted.
        /// 1.1 Ensure all watchlist subjects and groups are deleted.  
        /// </summary>
        public async Task Clean()
        {
            var groups = await service.Groups.GetAll();
            var s = await service.Subjects.DeleteAll();
            foreach (var group in groups.Items)
            {
                if (group.IsDefault) continue;
                var success = await service.Groups.DeleteById(group.Id);
            }
        }
        /// <summary>
        /// Delete one or more subjects.
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task DeleteSubjects(string[] ids)
        {
            var rep = await service.Subjects.Delete(ids);
        }
        /// <summary>
        /// Imports gamblers from a zip file. Images for a gambler is stored in a folder, that
        /// should have the name of the gambler.
        /// Uploading of gamblers are done in 3 stages: Preparation, Extraction from a zip file, check.
        /// 
        /// </summary>
        /// <param name="zipFile">A zip file with a folder structure and images</param>
        /// <param name="groupIds"></param>
        /// <returns></returns>
        public async Task<(bool, int)> UploadGamblers(string zipFile, List<string> groupIds)
        {
            if (!groupIds.Any()) throw new Exception($"{nameof(groupIds)} must have at least one group id");
            var prepare = new MassImportRequest
            {
                Name = "MassImport_" + DateTime.Now.ToString(CultureInfo.InvariantCulture).Substring(0),
                SubjectGroups = groupIds
            };
            var res = await service.Import.PrepareMassImport(prepare);
            var rep = await service.Import.UploadCompressedFile(res.UploadId, zipFile);
            await Task.Delay(TimeSpan.FromSeconds(1));
            var resultCheck = await service.GetUploadResult(res.UploadId);
            while (IsInProgress(resultCheck))
            {
                await Task.Delay(250);
                resultCheck = await service.GetUploadResult(res.UploadId);
            }
            return (resultCheck.Status.Equals("FINISHED"), 0);
        }
        /// <summary>
        /// I am afraid there will be some error codes.  
        /// </summary>
        /// <param name="resultCheck"></param>
        /// <returns></returns>
        private static bool IsInProgress(SenseWeb.AnyVision.Generated.UploadCompressedFileResult.Root resultCheck) =>
            new[] { "UPLOADING", "EXTRACTING", "PROCESSING" }.Contains(resultCheck.Status);
    }
}
