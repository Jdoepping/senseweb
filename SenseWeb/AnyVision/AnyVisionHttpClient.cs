﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Sense.AnyVision.ApiClasses;
using SenseWeb.AnyVision.interfaces;

namespace SenseWeb.AnyVision
{
    /// <summary>
    /// Adds token handling to an http client and extracts the json response into classes.
    /// All calls must have a token added to the authorization header.
    /// </summary>
    public class AnyVisionHttpClient : IAnyVisionHttpClient
    {
        private readonly TokenHandler tokenHandler;
        private readonly HttpClient client;
        private readonly Func<Task<HttpResponseMessage>> connect;

        public Action<string> Log { get; set; } = s => { };

        public AnyVisionHttpClient(HttpClient client, Func<Task<HttpResponseMessage>> connect) =>
            (this.client, this.connect, tokenHandler) = (client, connect, new TokenHandler());
        public async Task<T> PostAsync<T>(string requestUri, HttpContent content)=>
            await CallWrapper<T>(() => client.PostAsync(requestUri, content));
        public async Task<T> GetAsync<T>(string requestUri)=>
            await CallWrapper<T>(() => client.GetAsync(requestUri));

        public async Task<T> DeleteAsync<T>(string requestUri) =>
            await CallWrapper<T>(() => client.DeleteAsync(requestUri));

        public async Task<TR> PostAsJsonAsync<T, TR>(string requestUri, T param) =>
                await CallWrapper<TR>(() => client.PostAsJsonAsync(requestUri, param));
        public async Task<TR> PutAsJsonAsync<T, TR>(string requestUri, T param) =>
            await CallWrapper<TR>(()=>client.PutAsJsonAsync(requestUri, param));

        public async Task<TR> PatchAsJsonAsync<T, TR>(string requestUri, T param) =>
            await CallWrapper<TR>(() => client.PatchAsJsonAsync(requestUri, param));

        private async Task<T> CallWrapper<T>(Func<Task<HttpResponseMessage>> func)
        {
            await AddBearerToken();
            var response = await func();
            if (!response.IsSuccessStatusCode) await LogError(response);
            response.EnsureSuccessStatusCode();
            return await ExtractResponse<T>(response);
        }
       
        private async Task<T> ExtractResponse<T>(HttpResponseMessage response)
        {
            var content = await response.Content.ReadAsStringAsync();
            if (typeof(T) ==typeof(string)) return (T)(object)content;
            return (T)JsonConvert.DeserializeObject(content, typeof(T));
        }
        private async Task AddBearerToken() => await tokenHandler.AddToken(GetToken, AddToken);
        private async Task<string> GetToken()
        {
            var response = await connect();
            var token = await ExtractResponse<ConnectResponse>(response);
            return token.Token;
        }
        private void AddToken(string token) =>
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

        private async  Task LogError(HttpResponseMessage response)
        {
            var content = await response.Content.ReadAsStringAsync();
            Log(content);
        }
    }
}
