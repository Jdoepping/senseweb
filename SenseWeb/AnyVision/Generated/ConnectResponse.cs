﻿using Newtonsoft.Json;
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
namespace Sense.AnyVision.ApiClasses
{
    /// <summary>
    /// The response from an AnyVision login request
    /// </summary>
    public class ConnectResponse
    {
        [JsonProperty("token")] public string Token { get; set; }
        [JsonProperty("isEulaConfirmed")] public bool IsEulaConfirmed { get; set; }
    }
}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

