﻿using System.Text.Json.Serialization;

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
namespace SenseWeb.AnyVision.Generated
{
    // Root myDeserializedClass = JsonSerializer.Deserialize<MassUploadId>(myJsonResponse);
    public class MassUploadId
    {
        [JsonPropertyName("uploadId")]
        public string UploadId { get; set; }
    }
}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
