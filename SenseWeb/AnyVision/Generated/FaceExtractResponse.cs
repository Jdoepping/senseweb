﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
namespace SenseWeb.AnyVision.Generated.FaceExtractResponse
{
    // Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
    public class Attributes
    {
        [JsonPropertyName("gender")]
        public List<int> Gender { get; set; }

        [JsonPropertyName("age")]
        public List<string> Age { get; set; }

        [JsonPropertyName("glasses")]
        public List<bool> Glasses { get; set; }
    }

    public class Item
    {
        [JsonPropertyName("featuresQuality")]
        public int FeaturesQuality { get; set; }

        [JsonPropertyName("features")]
        public List<double> Features { get; set; }

        [JsonPropertyName("url")]
        public string Url { get; set; }

        [JsonPropertyName("objectType")]
        public int ObjectType { get; set; }

        [JsonPropertyName("landmarkScore")]
        public int LandmarkScore { get; set; }

        [JsonPropertyName("attributes")]
        public Attributes Attributes { get; set; }
    }

    public class Root
    {
        [JsonPropertyName("items")]
        public List<Item> Items { get; set; }
    }



}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
