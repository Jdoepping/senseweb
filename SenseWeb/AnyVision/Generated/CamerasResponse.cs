﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
namespace SenseWeb.AnyVision.Generated.Models.Cameras
{

    // Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
    public class FrameSkip
    {
        [JsonPropertyName("percent")]
        public int Percent { get; set; }

        [JsonPropertyName("autoSkipEnabled")]
        public bool AutoSkipEnabled { get; set; }
    }

    public class CameraPadding
    {
        [JsonPropertyName("top")]
        public string Top { get; set; }

        [JsonPropertyName("left")]
        public string Left { get; set; }

        [JsonPropertyName("right")]
        public string Right { get; set; }

        [JsonPropertyName("bottom")]
        public string Bottom { get; set; }
    }

    public class Configuration
    {
        [JsonPropertyName("webRTC")]
        public bool WebRTC { get; set; }

        [JsonPropertyName("preview")]
        public bool Preview { get; set; }

        [JsonPropertyName("frameSkip")]
        public FrameSkip FrameSkip { get; set; }

        [JsonPropertyName("cameraMode")]
        public List<int> CameraMode { get; set; }

        [JsonPropertyName("cameraPadding")]
        public CameraPadding CameraPadding { get; set; }

        [JsonPropertyName("ffmpegOptions")]
        public string FfmpegOptions { get; set; }

        [JsonPropertyName("frameRotation")]
        public int FrameRotation { get; set; }

        [JsonPropertyName("trackMaxLength")]
        public int TrackMaxLength { get; set; }

        [JsonPropertyName("trackMinLength")]
        public int TrackMinLength { get; set; }

        [JsonPropertyName("livenessThreshold")]
        public double LivenessThreshold { get; set; }

        [JsonPropertyName("enableFrameStorage")]
        public bool EnableFrameStorage { get; set; }

        [JsonPropertyName("trackerSeekTimeOut")]
        public int TrackerSeekTimeOut { get; set; }

        [JsonPropertyName("detectionMaxFaceSize")]
        public int DetectionMaxFaceSize { get; set; }

        [JsonPropertyName("detectionMinFaceSize")]
        public int DetectionMinFaceSize { get; set; }
    }

    public class LastError
    {
        [JsonPropertyName("code")]
        public int Code { get; set; }

        [JsonPropertyName("codeString")]
        public string CodeString { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }
    }

    public class Item
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("pipe")]
        public string Pipe { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("videoUrl")]
        public string VideoUrl { get; set; }

        [JsonPropertyName("configuration")]
        public Configuration Configuration { get; set; }

        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonPropertyName("isDeleted")]
        public bool IsDeleted { get; set; }

        [JsonPropertyName("deletedAt")]
        public object DeletedAt { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("threshold")]
        public double? Threshold { get; set; }

        [JsonPropertyName("timezone")]
        public string Timezone { get; set; }

        [JsonPropertyName("restriction")]
        public bool Restriction { get; set; }

        [JsonPropertyName("cameraGroupId")]
        public string CameraGroupId { get; set; }

        [JsonPropertyName("isEnabled")]
        public bool IsEnabled { get; set; }

        [JsonPropertyName("location")]
        public List<double> Location { get; set; }

        [JsonPropertyName("isLoadBalancingEnabled")]
        public bool IsLoadBalancingEnabled { get; set; }

        [JsonPropertyName("isAlternativeThresholdEnabled")]
        public bool IsAlternativeThresholdEnabled { get; set; }

        [JsonPropertyName("alternativeThreshold")]
        public object AlternativeThreshold { get; set; }

        [JsonPropertyName("timeProfileId")]
        public object TimeProfileId { get; set; }

        [JsonPropertyName("lastError")]
        public LastError LastError { get; set; }

        [JsonPropertyName("sdpFile")]
        public object SdpFile { get; set; }

        [JsonPropertyName("unAckCount")]
        public int UnAckCount { get; set; }
    }

    public class Root
    {
        [JsonPropertyName("items")]
        public List<Item> Items { get; set; }

        [JsonPropertyName("total")]
        public int Total { get; set; }
    }
}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
