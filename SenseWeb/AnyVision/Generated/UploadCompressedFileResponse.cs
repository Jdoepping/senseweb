﻿using System.Text.Json.Serialization;
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
namespace SenseWeb.AnyVision.Generated.UploadCompressedFile
{
    // Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
    public class Metadata
    {
    }

    public class Root
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("jobId")]
        public string JobId { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("mappedName")]
        public string MappedName { get; set; }

        [JsonPropertyName("subjectFolderName")]
        public string SubjectFolderName { get; set; }

        [JsonPropertyName("clientRelativePath")]
        public string ClientRelativePath { get; set; }

        [JsonPropertyName("storagePath")]
        public string StoragePath { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("metadata")]
        public Metadata Metadata { get; set; }

        [JsonPropertyName("createdDate")]
        public string CreatedDate { get; set; }
    }


}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
