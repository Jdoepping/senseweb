﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
namespace SenseWeb.AnyVision.Generated.Subject
{
    // NB Contains manual changes

    // Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
    public class Group
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("color")]
        public string Color { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("isActive")]
        public bool IsActive { get; set; }
    }

    public class Image
    {
        [JsonPropertyName("url")]
        public string Url { get; set; }

        [JsonPropertyName("isPrimary")]
        public bool IsPrimary { get; set; }

        [JsonPropertyName("objectType")]
        public int ObjectType { get; set; }

        [JsonPropertyName("validUntil")]
        public object ValidUntil { get; set; }
    }

    public class Item
    {
        [JsonPropertyName("groups")]
        public List<Group> Groups { get; set; }

        [JsonPropertyName("images")]
        public List<Image> Images { get; set; }

        /// <summary>
        /// Made datetime nullable
        /// </summary>
        [JsonPropertyName("lastIdentifiedAt")]
        public DateTime? LastIdentifiedAt { get; set; }

        [JsonPropertyName("timesIdentified")]
        public int TimesIdentified { get; set; }

        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("subjectSource")]
        public int SubjectSource { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("isIgnored")]
        public bool IsIgnored { get; set; }

        [JsonPropertyName("isLocked")]
        public bool IsLocked { get; set; }
    }

    public class Root
    {
        [JsonPropertyName("items")]
        public List<Item> Items { get; set; }

        [JsonPropertyName("total")]
        public int Total { get; set; }
    }


}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
