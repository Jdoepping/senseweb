﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
namespace SenseWeb.AnyVision.Generated.AddSubject
{
    // Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
    public class Group
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("th")]
        public int Th { get; set; }

        [JsonPropertyName("type")]
        public int Type { get; set; }

        [JsonPropertyName("color")]
        public string Color { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("isActive")]
        public bool IsActive { get; set; }

        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonPropertyName("deletedAt")]
        public object DeletedAt { get; set; }

        [JsonPropertyName("isDefault")]
        public bool IsDefault { get; set; }

        [JsonPropertyName("isDeleted")]
        public bool IsDeleted { get; set; }

        [JsonPropertyName("alertLevel")]
        public string AlertLevel { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }
    }

    public class Attributes
    {
    }

    public class Image
    {
        [JsonPropertyName("url")]
        public string Url { get; set; }

        [JsonPropertyName("features")]
        public List<double> Features { get; set; }

        [JsonPropertyName("isPrimary")]
        public bool IsPrimary { get; set; }

        [JsonPropertyName("attributes")]
        public Attributes Attributes { get; set; }

        [JsonPropertyName("featuresId")]
        public string FeaturesId { get; set; }

        [JsonPropertyName("objectType")]
        public int ObjectType { get; set; }

        [JsonPropertyName("validUntil")]
        public DateTime ValidUntil { get; set; }

        [JsonPropertyName("landmarkScore")]
        public object LandmarkScore { get; set; }

        [JsonPropertyName("featuresQuality")]
        public int FeaturesQuality { get; set; }
    }

    public class Root
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("subjectSource")]
        public int SubjectSource { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("isIgnored")]
        public bool IsIgnored { get; set; }

        [JsonPropertyName("isLocked")]
        public bool IsLocked { get; set; }

        [JsonPropertyName("lastIdentifiedAt")]
        public object LastIdentifiedAt { get; set; }

        [JsonPropertyName("timesIdentified")]
        public int TimesIdentified { get; set; }

        [JsonPropertyName("groups")]
        public List<Group> Groups { get; set; }

        [JsonPropertyName("otherImages")]
        public List<object> OtherImages { get; set; }

        [JsonPropertyName("image")]
        public Image Image { get; set; }
    }
}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
