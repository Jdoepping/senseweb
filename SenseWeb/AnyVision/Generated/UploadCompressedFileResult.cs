﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
namespace SenseWeb.AnyVision.Generated.UploadCompressedFileResult
{
    // NB NB This file has manual corrections (DateTime set to DateTime?)

    // Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
    public class Metadata
    {
        [JsonPropertyName("threshold")]
        public int Threshold { get; set; }

        [JsonPropertyName("subjectGroups")]
        public List<string> SubjectGroups { get; set; }

        [JsonPropertyName("isSearchBackwards")]
        public bool IsSearchBackwards { get; set; }
    }

    public class FilesMetrics
    {
        [JsonPropertyName("uploaded")]
        public int Uploaded { get; set; }

        [JsonPropertyName("waitingForPipe")]
        public int WaitingForPipe { get; set; }

        [JsonPropertyName("waitingForSubjectService")]
        public int WaitingForSubjectService { get; set; }

        [JsonPropertyName("failed")]
        public int Failed { get; set; }

        [JsonPropertyName("finished")]
        public int Finished { get; set; }

        [JsonPropertyName("totalFiles")]
        public int TotalFiles { get; set; }
    }

    public class Root
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonPropertyName("updatedAt")]
        public DateTime UpdatedAt { get; set; }

        /// <summary>
        /// Making it nullable was added manually
        /// </summary>
        [JsonPropertyName("endDate")]
        public DateTime? EndDate { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("storagePath")]
        public string StoragePath { get; set; }

        [JsonPropertyName("reportUrl")]
        public object ReportUrl { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("metadata")]
        public Metadata Metadata { get; set; }

        [JsonPropertyName("progress")]
        public double? Progress { get; set; }

        [JsonPropertyName("filesMetrics")]
        public FilesMetrics FilesMetrics { get; set; }
    }


}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
