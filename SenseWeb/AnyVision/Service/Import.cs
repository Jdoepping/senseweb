﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using SenseWeb.AnyVision.ApiClasses;
using SenseWeb.AnyVision.Generated;
using SenseWeb.AnyVision.interfaces;

namespace SenseWeb.AnyVision.Service
{
    public class Import
    {
        private IAnyVisionHttpClient Client { get; }
        public Import(IAnyVisionHttpClient client) => Client = client;

        public async Task<MassUploadId> PrepareMassImport(MassImportRequest prepare)
        {
            return await Client.PostAsJsonAsync<MassImportRequest, MassUploadId>("upload/prepare/mass-import", prepare);
        }
        public async Task<bool> UploadCompressedFile(string id, string fileName)
        {
            // If contentType is set on a form, then it will not have boundary
            // and the file will not be read! 
            var form = new MultipartFormDataContent();
            //form.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
            //form.Headers.ContentType = MediaTypeHeaderValue.Parse("application/zip");
            var fileContent = new ByteArrayContent(File.ReadAllBytes(fileName));
            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/zip");
            //fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
            fileContent.Headers.ContentDisposition = ContentDispositionHeaderValue.Parse(@"form-data; name=file; filename=uploadit.zip");
            form.Add(fileContent);
            var path = $"upload/extract/{id}";
            var response = await Client.PostAsync<string>(path, form);
            return response.Equals("OK");
        }
    }
}
