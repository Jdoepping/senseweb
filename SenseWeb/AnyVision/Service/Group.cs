﻿using System.Net.Http;
using System.Threading.Tasks;
using SenseWeb.AnyVision.interfaces;

namespace SenseWeb.AnyVision.Service
{
    /// <summary>
    /// A group is a container for subjects.
    /// A group is often referred to as a watchlist.
    /// </summary>
    public class Group
    {
        private IAnyVisionHttpClient Client { get; }
        public Group(IAnyVisionHttpClient client) => Client = client;

        /// <returns>All groups</returns>
        public async Task<Generated.Group.Root> GetAll() =>
            await Client.GetAsync<SenseWeb.AnyVision.Generated.Group.Root>($"groups");

        /// <summary>
        /// Create a group
        /// </summary>
        /// <param name="group">The created group</param>
        /// <returns></returns>
        public async Task<Generated.Group.Item> Create(Generated.Group.CreateGroup group) =>
            await Client.PostAsJsonAsync<Generated.Group.CreateGroup, Generated.Group.Item>("groups", group);

        /// <returns>Group properties</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">Returns 404, if the id doesn't match a group</exception>
        public async Task<Generated.Group.Item> GetById(string id) =>
            await Client.GetAsync<Generated.Group.Item>($"groups/{id}");
        public async Task<bool> DeleteById(string id)
        {
            var response =  await Client.DeleteAsync<string>($"groups/{id}");
            return response.Equals("OK");
        }

        /// <summary>
        /// Removes all the subjects belonging to the group with id <param name="groupId"></param>
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public async Task<bool> ClearAllSubjects(string groupId)
        {
            var response = await Client.PostAsync<string>($"groups/{groupId}/clear-all-subjects", new StringContent(""));
            return response.Equals("OK");
        }

        //public async Task<HttpResponseMessage> RemoveSubjectsGroup(CreateGroup group)
        //{
        //    var response = await Client.PostAsJsonAsync("groups", group);
        //    return response;
        //}
    }
}
