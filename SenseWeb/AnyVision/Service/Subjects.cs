﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SenseWeb.AnyVision.ApiClasses;
using SenseWeb.AnyVision.interfaces;

namespace SenseWeb.AnyVision.Service
{
    /// <summary>
    /// Deals with Subjects
    /// Subjects are members of Groups. 
    /// </summary>
    public class Subjects
    {
        private IAnyVisionHttpClient Client { get; }
        public Subjects(IAnyVisionHttpClient client) => Client = client;
        /// <summary>
        /// Returns subjects, with a paging mechanism (I guess, so correct it if it is wrong) 
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="sortOrder"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<Generated.Subject.Root> Get(int offset = 0, string sortOrder = "desc", int limit = 1000) =>
            await Client.GetAsync<Generated.Subject.Root>($"subjects?offset={offset}&sortOrder={sortOrder}&limit={limit}");

        /// <summary>
        /// Returns the subjects belonging to the group with group id <exception cref="groupId"></exception>
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Generated.Subject.Item>> GetFromGroup(string groupId)
        {
            var all = await  Get();
            return all.Items.Where(g => g.Groups.Exists(t => t.Id.Equals(groupId)));
        }

        /// <summary>
        /// Adding a subject. The information needed to add requires other calls
        /// to be run first. An image is needed and 
        /// </summary>
        /// <param name="subject"></param>
        /// <returns></returns>
        public async Task<bool> Add(AddSubject subject)
        {
            var res = await Client.PostAsJsonAsync<AddSubject, Generated.AddSubject.Root>("subjects", subject);
            return true;
        }
        /// <summary>
        /// Updates a subject
        /// </summary>
        /// <param name="id">The id of the subject</param>
        /// <param name="subject">Properties for the subject</param>
        /// <returns></returns>
        public async Task<bool> Patch(string id, PatchSubject subject)
        {
            var res = await Client.PatchAsJsonAsync<PatchSubject, string>($"subjects/{id}", subject);
            return true;
        }

        /// <summary>
        /// Deletes the subjects identified by the ids
        /// Success is a return code of 204. 
        /// </summary>
        /// <param name="ids">List of UUIDs identifying the subjects to be deleted</param>
        /// <returns></returns>
        public async Task<bool> Delete(string[] ids)
        {
            if (ids.Length == 0) return true;
            // Single delete doesn't seem to work
            //if (ids.Length == 1) return await DeleteSingle(ids.First());
            var list = new DeleteList
            {
                Subjects = ids.ToArray()
            };
            var res = await Client.PostAsJsonAsync<DeleteList, string>("subjects/bulk-delete", list);
            return true;
        }

        public async Task<bool> DeleteSingle(string id)
        {
            var res  =await Client.DeleteAsync<string>($"subjects/{id}");
            return res.Equals("OK");
        }

        /// <summary>
        /// Deletes all subjects
        /// Success is a return code of 204. 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAll()
        {
            var res = await Client.PostAsync<string>("subjects/bulk-delete/all", new StringContent(""));
            return true;
        }

        /// <summary>
        /// Moves the the subjects identified by <param name="subjectIds"/> to the groups
        /// identified by <param name="groupIds"/>
        /// </summary>
        /// <returns></returns>
        public async Task<string> ReGroupSubjects(List<string> groupIds, List<string> subjectIds)
        {
            var body = new ReGroupBody
            {
                subjectGroupIds = groupIds.ToArray(),
                subjectIds = subjectIds.ToArray()
            };
            var response = await Client.PutAsJsonAsync<ReGroupBody, string>("subjects/regroup", body);
            return response;
        }
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public class ReGroupBody
        {
            public string[] subjectIds { get; set; }
            public string[] subjectGroupIds { get; set; }
        }

        private class DeleteList
        {
            /// <summary>
            /// A list of UUID's for subjects to delete.
            /// </summary>
            [JsonProperty(PropertyName = "ids")]
            public string[] Subjects { get; set; }
        }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    }
}
