﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SenseWeb.AnyVision.ApiClasses;
using SenseWeb.AnyVision.Generated;
using SenseWeb.AnyVision.interfaces;

namespace SenseWeb.AnyVision.Service
{
    /// <summary>
    /// Todo: Rename
    /// </summary>
    public class Service
    {
        private const string AnyVisionPath = "172.20.48.78/bt/api/";
        //private readonly string url;
        private TokenHandler tokenHandlerHandler;
        private readonly HttpClient client;
        public Action<string> Log { get=> Client.Log; set=>Client.Log= value; }
        public IAnyVisionHttpClient Client { get; }

        public Service(HttpClient client/*, Action<string> log */)
        {
            client.BaseAddress = new Uri($"https://{AnyVisionPath}");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            this.client = client;
            //this.log = log;
            Client = new AnyVisionHttpClient(client, ()=>Connect("jakob", "Anyvision1!"));
            tokenHandlerHandler = new TokenHandler();
            Subjects = new Subjects(Client);
            Groups = new Group(Client);
            Import = new Import(Client);
        }

        public Subjects Subjects { get; }
        public Group Groups { get; }
        public Import Import { get; }

        public async Task<HttpResponseMessage> Connect(string username, string password)
        {
            var response = await client.PostAsJsonAsync("login", new Credentials
            {
                Username = username,
                Password = password
            });
            response.EnsureSuccessStatusCode();
            return response;
        }
        public async Task<Generated.Models.Cameras.Root> Cameras(int offset = 0, string sortOrder = "desc", int limit = 10) =>
            await Client.GetAsync<Generated.Models.Cameras.Root>($"cameras?offset={offset}&sortOrder={sortOrder}&limit={limit}");
      
        public async Task<Generated.UploadCompressedFileResult.Root> GetUploadResult(string uploadId) =>
            await Client.GetAsync<Generated.UploadCompressedFileResult.Root>($"mass-import/{uploadId}");
       
        public async Task<Generated.FaceExtractResponse.Root> ExtractFacesFromFile(string filePath)
        {
            var form = new MultipartFormDataContent();
            var fileContent = new ByteArrayContent(File.ReadAllBytes(filePath));
            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
            form.Add(fileContent, "file", Path.GetFileName(filePath));
            var path = "external-functions/extract-faces-from-image";
            var response = await Client.PostAsync<Generated.FaceExtractResponse.Root>(path, form);
            return response;
        }

        //public static string GetBitmapAsBase64String(SixLabors.ImageSharp.Image image)
        //{
        //    return null;//image.ToBase64String();
        //}


        private class Credentials
        {
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
            [JsonProperty("username")]
            public string Username { get; set; }
            [JsonProperty("password")]
            public string Password { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        }
    }
}
