﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SenseWeb.AnyVision
{
    public static class AnyVisionHttpClientExtensions
    {
        /// <summary>
        /// Created this extension because earlier versions of HttpClient apparently had this.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="client"></param>
        /// <param name="requestUri"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> PostAsJsonAsync<T>(this HttpClient client, string requestUri, T param) => 
            await Helper((c) => client.PostAsync(requestUri, c), param);

        public static async Task<HttpResponseMessage> PutAsJsonAsync<T>(this HttpClient client, string requestUri, T param)=>
            await Helper((c) => client.PutAsync(requestUri, c), param);
        public static async Task<HttpResponseMessage> PatchAsJsonAsync<T>(this HttpClient client, string requestUri, T param) =>
            await Helper((c) => client.PatchAsync(requestUri, c), param);

        private static async Task<HttpResponseMessage> Helper<T>(Func<StringContent, Task<HttpResponseMessage>> func, T param)
        {
            var json = JsonConvert.SerializeObject(param);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return await func(content);
        }
    }
}